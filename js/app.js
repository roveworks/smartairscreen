var App = function() {

    function handleViewport() {
        $("input, textarea, select, button, i, div.note-editing-area, span.select2-selection, .calendar-time, ul.tag-editor, div.asSpinner-control").on({
            'touchstart': function() {
                zoomDisable();
            }
        });
        $("input, textarea, select, button, i, div.note-editing-area, span.select2-selection, .calendar-time, ul.tag-editor, div.asSpinner-control").on({
            'touchend': function() {
                setTimeout(zoomEnable, 500);
            }
        });

        function zoomDisable() {
            $('head meta[name=viewport]').remove();
            $('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">');
        }

        function zoomEnable() {
            $('head meta[name=viewport]').remove();
            $('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1">');
        }
    }

    function handleIEFixes() {
        //fix html5 placeholder attribute for ie7 & ie8
        if (jQuery.browser.msie && jQuery.browser.version.substr(0, 1) < 9) { // ie7&ie8
            $('input[placeholder], textarea[placeholder]').each(function() {
                var input = jQuery(this);
                $(input).val(input.attr('placeholder'));
                $(input).focus(function() {
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                });
                $(input).blur(function() {
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                        input.val(input.attr('placeholder'));
                    }
                });
            });
        }
    }

    function handleBootstrap() {
        /*Bootstrap Carousel*/
        $('.carousel').carousel({
            interval: 15000,
            pause: 'hover'
        });
        /*Tooltips*/
        $('.tooltips').tooltip();
        $('.tooltips-show').tooltip('show');
        $('.tooltips-hide').tooltip('hide');
        $('.tooltips-toggle').tooltip('toggle');
        $('.tooltips-destroy').tooltip('destroy');
        /*Popovers*/
        $('.popovers').popover();
        $('.popovers-show').popover('show');
        $('.popovers-hide').popover('hide');
        $('.popovers-toggle').popover('toggle');
        $('.popovers-destroy').popover('destroy');
    }

    function handleToggle() {
        $('.list-toggle').on('click', function() {
            $(this).toggleClass('active');
        });
    }

    function handleSticky() {
        var stickyAddClass = function() {
            $("#header-fixed").addClass("header-fixed-trans");
            $(".header-sticky-space").addClass("header-fixed-space-trans");
        }
        var stickyRemoveClass = function() {
            $("#header-fixed").removeClass("header-fixed-trans");
            $(".header-sticky-space").removeClass("header-fixed-space-trans");
        }
        $(window).scroll(function() {
            if ($("#header-fixed").hasClass("fixed-main")) {
                if ($(window).scrollTop() > 66) { stickyAddClass(); } else { stickyRemoveClass(); }
            } else {
                if ($(window).scrollTop() > 66) { stickyAddClass(); } else { stickyRemoveClass(); }
            }
        });
    }

    function handleSidebar() {
        var sides = ["left", "top", "right", "bottom"];
        for (var i = 0; i < sides.length; ++i) {
            var cSide = sides[i];
            $(".sidebar." + cSide).sidebar({ side: cSide });
        }
        $(".sidebar-left-trigger[data-action]").on("click", function() {
            var $this = $(this);
            var action = $this.attr("data-action");
            var side = $this.attr("data-side");
            $(".sidebar." + side).trigger("sidebar:" + action);
            $("html").toggleClass("overflow-hidden");
            $(".sidebar-left-mask, .sidebar-left-content").toggleClass("active");
            return false;
        });
        $(".sidebar-right-trigger[data-action]").on("click", function() {
            var $this = $(this);
            var action = $this.attr("data-action");
            var side = $this.attr("data-side");
            $(".sidebar." + side).trigger("sidebar:" + action);
            $("html").toggleClass("overflow-hidden");
            $(".sidebar-right-mask").toggleClass("active");
            return false;
        });
        setTimeout(function() {
            $(".sidebar").show();
        }, 500);
        $(window).resize(function() {
            $(".sidebar").show();
        });
    }

    function handleBackToTop() {
        $(document).ready(function() {
            $(".back-to-top").addClass("hidden-top");
            $(window).scroll(function() {
                if ($(this).scrollTop() === 0) {
                    $(".back-to-top").addClass("hidden-top")
                } else {
                    $(".back-to-top").removeClass("hidden-top")
                }
            });

            $('.back-to-top').click(function() {
                $('body,html').animate({ scrollTop: 0 }, 1200);
                return false;
            });

            $('.btn-fix-navi-top').click(function() {
                $('body,html').animate({ scrollTop: 0 }, 1200);
                return false;
            });

            $('.quick-scroll-btn.top-btn').click(function() {
                $('body,html').animate({ scrollTop: 0 }, 1200);
                return false;
            });

            $('.quick-scroll-btn.down-btn').click(function() {
                $('body,html').animate({ scrollTop: $(document).height() }, 1200);
                return false;
            });
        });
    }

    // 992px 이상에서 1차메뉴 동일하게 분할하기
    function handleNavList() {
        $(document).ready(function() {
            var navList, navListLength, navListDivision;
            navList = $(".nav.navbar-nav > .division");
            navListLength = navList.length;
            navListDivision = 100 / navListLength;
            navList.css("width", navListDivision + "%")
        });
    }

    function handleFamilySite() {
        $(".family-site dt a").click(function() {
            $(".family-site dd").slideToggle(300);
            return false;
        });

                $(".family-site2 dt a").click(function() {
            $(".family-site2 dd").slideToggle(300);
            return false;
        });
    }

    function handleFixNavi() {
        $(".btn-fix-navi").click(function() {
            $(".fix-navi").toggleClass("active");
        });
    }

    return {
        init: function() {
            handleViewport();
            handleBootstrap();
            handleIEFixes();
            handleToggle();
            handleSticky();
            handleSidebar();
            handleBackToTop();
            handleNavList();
            handleFamilySite();
            handleFixNavi();
        }
    };

}();
/* 반응형 테이블 */
/**
 * Simplify the creation of a multi dimensional array
 * */
function createEmptyArray(rowN, colN) {
    var emptyArray = new Array(rowN);

    for (var i = 0; i < emptyArray.length; i++) {
        emptyArray[i] = new Array(colN);
    }

    return emptyArray;
}

/*
find the exactly number of col
*/
function calculateNumberOfCol(tableN) {
    var headerRow = document.querySelectorAll("table.makeit_rwd:nth-child(" + tableN + ") thead tr:nth-child(1) th"),
        colN = 0,
        i;

    for (i = 0; i < headerRow.length; i++) {
        colN += headerRow[i].colSpan;
    }

    return colN;
}

/*
find the next cell that is the real one to add the header
*/
function findNextFreeBodyCol(headersText, notFreeCol, currentRow, currentCol) {
    while (currentCol < headersText[currentRow - 1].length) {
        if (notFreeCol.indexOf((currentRow * 10) + currentCol) === -1) {
            return currentCol;
        }

        currentCol++;
    }
}

/**
 * find the next cell in the headers array that not contains a value
 * */
function findNextFreeCol(headersText, rowN, colN) {
    while (colN < headersText[rowN - 1].length) {
        if (!headersText[rowN - 1][colN]) {
            return colN;
        }

        colN++;
    }
}

/**
 * add the content to all cell specify in the array headers
 * */
function filledHeadersTextArray(headersText, currentCol, headerRowN, th, textContent) {
    for (var rowPos = headerRowN; rowPos < (th.rowSpan + headerRowN); rowPos++) {
        for (var colPos = currentCol; colPos < currentCol + th.colSpan; colPos++) {
            headersText[rowPos - 1][colPos] = textContent;
        }
    }
}

/**
 * add the cells that are not free due to a rowspan/colspan
 * */
function filledNotFreeColArray(nbOfHeaderRow, notFreeCol, currentRow, currentCol, td) {
    //begin to the next line
    for (var rowPos = currentRow + 1; rowPos <= (currentRow + (td.rowSpan - 1)); rowPos++) {
        for (var colPos = currentCol; colPos < currentCol + td.colSpan; colPos++) {
            notFreeCol.push((rowPos * 10) + colPos); //representation for the first cell is 10 (1 * 10) + 0
        }
    }
}

/**
 * add data-th for table n
 * @param tableN the position of the table in the dom
 */
function addTableHeader(tableN) {
    var nbOfHeaderRow = document.querySelectorAll("table.makeit_rwd:nth-child(" + tableN + ") thead tr").length,
        headers,
        headerRowN, headerColN;

    //create an array of the good dimension
    var headersText = createEmptyArray(nbOfHeaderRow, calculateNumberOfCol(tableN));

    for (headerRowN = 1; headerRowN <= nbOfHeaderRow; headerRowN++) {
        headers = document.querySelectorAll("table.makeit_rwd:nth-child(" + tableN + ") thead tr:nth-child(" + headerRowN + ") th");

        var currentCol = 0;
        for (headerColN = 0; headerColN < headers.length; headerColN++) {
            currentCol = findNextFreeCol(headersText, headerRowN, currentCol); //a cell that not contains a content

            var th = headers[headerColN];
            var textContent = th.textContent.replace(/\r?\n|\r/, ""); //get the title

            if (th.colSpan > 1 || th.rowSpan > 1) {
                //add the title to all cell concerned by the rowspan colspan
                filledHeadersTextArray(headersText, currentCol, headerRowN, th, textContent);
            } else {
                headersText[headerRowN - 1][currentCol] = textContent;
            }

            currentCol += th.colSpan;
        }
    }

    console.log(headersText);

    addTableHeaderToRows(tableN, nbOfHeaderRow, headersText);
}

/**
 * add data-th for rows concerned by header x of the table n
 * @param tableN
 * @param nbOfHeaderRow
 * @param headersText
 */
function addTableHeaderToRows(tableN, nbOfHeaderRow, headersText) {
    var nbOfBodyRow = document.querySelectorAll("table.makeit_rwd:nth-child(" + tableN + ") tbody tr").length,
        rowN, colN, row, td;

    var notFreeCol = [],
        currentCol, currentRow, currentBlock = 1;

    currentRow = 1;

    for (rowN = 1; rowN <= nbOfBodyRow; rowN++) {
        row = document.querySelector("table.makeit_rwd:nth-child(" + tableN + ") tbody tr:nth-child(" + rowN + ")");

        //add a class for define a block
        row.setAttribute("class", (row.getAttribute("class") || '') + (currentBlock % 2 === 0 ? " makeit_rwd_block_odd " : " makeit_rwd_block_even "));

        currentCol = 0;

        for (colN = 0; colN < row.cells.length; colN++) {
            currentCol = findNextFreeBodyCol(headersText, notFreeCol, currentRow, currentCol);

            td = row.cells[colN];

            if (td.innerText.trim().length === 0) {
                td.innerHTML = '&nbsp;'; //help to a better style
            }

            //                    if (rowN % nbOfHeaderRow === 1) {
            if (currentRow === 1 && colN === 0) {
                td.setAttribute("class", (td.getAttribute("class") || '') + " makeit_rwd_first_col");
            }

            td.setAttribute("data-th", headersText[currentRow - 1][currentCol] || ''); //add the correct title on the cell

            if (td.rowSpan > 1) {
                console.log(currentRow, currentCol, td);
                filledNotFreeColArray(nbOfHeaderRow, notFreeCol, currentRow, currentCol, td);
            }

            currentCol += td.colSpan;
        }

        //we processed the entire content (if header row is 3, the content is on 3 row) so we reinitialize vars
        if (currentRow % nbOfHeaderRow === 0) {
            currentRow = 1;
            currentBlock++;
            console.log(nbOfHeaderRow, notFreeCol);
            notFreeCol = [];
        } else {
            currentRow++;
        }
    }
}

/**
 * add data-th for all the table of the dom
 */
function addTableHeaders() {
    var nbOfTableFsoRwd = document.querySelectorAll("table.makeit_rwd").length,
        tableN;

    for (tableN = 1; tableN <= nbOfTableFsoRwd; tableN++) {
        addTableHeader(tableN);
    }
}

//When the document is ready, responsive tables
document.addEventListener("DOMContentLoaded", function() {
    addTableHeaders();
});